package al.stepan.wm;

import java.util.HashMap;
import java.util.Map;

/**
 * Represents a weighted set. 
 * When an item is added repeatedly, it's weight in the set is increasing.
 * This influences the expectation of how often this item will be returned from the set using getWeightedRandom().
 * For example: we perform add("yes") twice, and add("no") once. The set will return "yes" twice as often as "no".
 * By the law of large numbers, the more you sample the test, the closer will number of "yes"s come to 2/3, and number of "no"s to 1/3.
 * The set utilizes array as cache, and consecutive calls to getWeightedRandom() are run in constant time: O(1).
 * Any call to add(), requires cache to be rebuild lazily. 
 * First call to getWeightedRandom() after a call to add(), will require time proportional to the size of the set: O(n).
 * 
 * @author stepan
 *
 */
public class WeightedSet {

	private Map<String, Integer> map = new HashMap<>();
	private int size = 0;
	private String[] array;
	private boolean modified = false;

	public void add(String word) {
		if (map.containsKey(word)) {
			int currentCount = map.get(word);
			map.put(word, currentCount + 1);
		} else {
			map.put(word, 1);
		}
		size++;
		modified = true;
	}

	public String getWeightedRandom() {
		int randomIndex = (int) (Math.random() * size);
		if (modified) {
			rebuildArray();
			modified = false;
		}
		return array[randomIndex];
	}

	private void rebuildArray() {
		String[] result = new String[size];
		int index = 0;
		for (String word : map.keySet()) {
			for (int i = map.get(word); i > 0; i--) {
				result[index++] = word;
			}
		}
		array = result;
	}

	public int totalWeights() {
		return size;
	}

	public int uniqueWords() {
		return map.keySet().size();
	}

	@Override
	public String toString() {
		return map.toString();
	}

}
