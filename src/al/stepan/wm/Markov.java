package al.stepan.wm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementation of Markov chain algorithm for text.
 * Utilizes HashMap with key type String, representing current prefix, and value type WeightedSet, representing suffixes.
 * Supports two main operations: building of suffix table, and building text based on that table.
 * 
 * @author stepan
 *
 */
public class Markov {

	private Map<String, WeightedSet> map = new HashMap<>();
	private static final String SPECIAL_WORD = "_not_a_word_";
	private SizedQueue prefix;
	private List<String> text;
	private int prefixLength;

	/**
	 * Public constructor.
	 * @param text List<String> containing text from which prefix-suffixes table is build.
	 * @param prefixLength int representing the length of a prefix(chain).
	 */
	public Markov(List<String> text, int prefixLength) {
		this.text = text;
		this.prefixLength = prefixLength;
		prefix = new SizedQueue(prefixLength);
	}

	/**
	 * Builds prefix-suffixes table based on the text provided to constructor, taking into account suffix length.
	 */
	public void buildSuffixTable() {
		prepare(text);
		for (int i = 0; i < text.size() - 1; i++) {
			prefix.add(text.get(i));
			if (!map.containsKey(prefix.toString())) {
				map.put(prefix.toString(), new WeightedSet());
			}
			map.get(prefix.toString()).add(text.get(i + 1));
		}
	}

	/**
	 * Builds the text transformed using the table build by buildSuffixTable().
	 * @return text String representing the final output of the algorithm.
	 */
	public String buildText() {
		StringBuilder sb = new StringBuilder();
		SizedQueue prefix = firstKey();
		String suffix = map.get(prefix.toString()).getWeightedRandom();
		while (!suffix.equals(SPECIAL_WORD)) {
			sb.append(suffix).append(" ");
			prefix.add(suffix);
			suffix = map.get(prefix.toString()).getWeightedRandom();
		}
		return sb.toString();
	}
	
	/**
	 * Convenience method that builds a prefix-suffix table and builds transformed text.
	 * @return transformed text String.
	 */
	public String transform(){
		buildSuffixTable();
		return buildText();
	}

	/**
	 * Regenerates the first key to the table.
	 * @return key String representing first key.
	 */
	private SizedQueue firstKey() {
		SizedQueue prefix = new SizedQueue(prefixLength);
		for (int i = 0; i < prefixLength; i++) {
			prefix.add(SPECIAL_WORD);
		}
		return prefix;
	}

	/**
	 * Puts a number of special words in front of the text. The number of
	 * special words added is equal to prefix length. This allows us to have a
	 * starting point for our algorithm. Appends one special word at the end of
	 * the text. This allows us to have an ending point during text generation.
	 * 
	 * @param text
	 *            List<String> to be prepared.
	 */
	private void prepare(List<String> text) {
		for (int i = 0; i < prefixLength; i++) {
			text.add(0, SPECIAL_WORD);
		}
		text.add(text.size(), SPECIAL_WORD);
	}

}
