package al.stepan.wm;

import java.util.LinkedList;
import java.util.List;

/**
 * A Queue that is limited in size. When a queue size is reached, adding new item pushes one off the front of the queue, maintaining the size.
 * Initial size cannot be changed. 
 * This class supports one main operation: adding (enqueue operation). 
 * Adding when queue size will exceed allowed size, will cause front of the queue to drop one element.
 * For simplicity, does not implement java.util.Queue.
 * Between LinkedList and ArrayList, the decision was made to use LinkedList. 
 * The use case for the queue appears to favor deletion of the top and appending at the end: both of which (doubly) LinkedList can do in constant time O(1).
 * 
 * @author stepan
 *
 */
public class SizedQueue {

	private int size;
	private List<String> queue = new LinkedList<>();

	public SizedQueue(int size) {
		this.size = size;
	}

	public void add(String next) {
		while (queue.size() >= size) {
			queue.remove(0);
		}
		queue.add(next);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (String s : queue) {
			sb.append(s).append(" ");
		}
		return sb.substring(0, sb.length() - 1);// String without the final space character.
	}

}
