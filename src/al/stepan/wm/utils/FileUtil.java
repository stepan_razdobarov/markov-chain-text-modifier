package al.stepan.wm.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class FileUtil {
	/**
	 * Reads contents of a file and returns it as List<String> using space character as a separator.
	 * @param fileName String
	 * @return content List<String> with contents of the file.
	 */
	public static List<String> readTextFrom(String fileName) {
		Scanner scan = new Scanner("file not found");
		try {
			scan = new Scanner(new File(fileName));
		} catch (FileNotFoundException e) {
			System.out.println("File named " + fileName
					+ " could not be found. Please enter a valid file name.");
			e.printStackTrace();
		}
		StringBuilder sb = new StringBuilder();
		while (scan.hasNext()) {
			sb.append(scan.nextLine());
		}

		List<String> content = new ArrayList<>();
		content.addAll(Arrays.asList(sb.toString().split(" ")));
		return content;
	}
}
