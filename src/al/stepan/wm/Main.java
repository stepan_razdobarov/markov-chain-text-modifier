package al.stepan.wm;

import al.stepan.wm.utils.FileUtil;

public class Main {
	/**
	 * Runs one Markov text transformation.
	 * Uses command line arguments: fileName and prefixLength to initialize Markov instance.
	 * Prints the transformed text to System.out.
	 * @param args String fileName and int prefixLength
	 */
	public static void main(String[] args) {
		if(args.length < 2) throw new IllegalArgumentException("Two parameters are required: fileName:String and prefixLength:int.");
		String fileName = args[0];
		try {
			int suffixLength = Integer.valueOf(args[1]);
			String result = new Markov(FileUtil.readTextFrom(fileName), suffixLength).transform();
			System.out.println(result);
		} catch (NumberFormatException nfe) {
			System.out.println("Please enter a valid number as a second parameter.");
		}
	}
}


