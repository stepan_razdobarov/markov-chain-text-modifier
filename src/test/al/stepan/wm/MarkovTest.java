package test.al.stepan.wm;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import al.stepan.wm.Markov;

public class MarkovTest {

	private Markov markov;
	private List<String> text;
	private String testText = "One of the most common types of advice we give at Y Combinator is to do things that don't scale. "
			+ "A lot of would-be founders believe that startups either take off or don't. You build something, make it available, "
			+ "and if you've made a better mousetrap, people beat a path to your door as promised. Or they don't, in which case "
			+ "the market must not exist.";

	@Before
	public void setUp() throws Exception {
		text = new ArrayList<>();
		for (String each : testText.split(" ")) {
			text.add(each);
		}
		markov = new Markov(text, 1);
	}

	@Test
	public void should_begin_with_the_same_word() {
		markov.buildSuffixTable();
		assertEquals(markov.buildText().split(" ")[0],  testText.split(" ") [0]);
	}
	
	@Test
	public void should_end_with_the_same_word() {
		markov.buildSuffixTable();
		String[] originalWords = testText.split(" ");
		String[] generatedWords = markov.buildText().split(" ");
		assertEquals(originalWords[originalWords.length-1],  generatedWords[generatedWords.length-1]);
	}

	
}
