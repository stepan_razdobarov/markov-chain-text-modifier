package test.al.stepan.wm;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import al.stepan.wm.SizedQueue;

public class SizedQueueTest {

	private static final int QUEUE_SIZE = 7;
	private SizedQueue queue;
	private String[] strings;

	@Before
	public void setUp() throws Exception {
		queue = new SizedQueue(QUEUE_SIZE);
		strings = "abcdefghijklmnopqrstuvwxyz".split("");
	}

	@Test
	public void should_be_fifo() {
		for (String s : strings) {
			queue.add(s);
		}
		assertEquals(queue.toString(), "t u v w x y z");
	}

	@Test
	public void should_keep_fixed_size_once_filled() {
		for (String s : strings) {
			queue.add(s);
		}
		assertEquals(queue.toString().split(" ").length, QUEUE_SIZE);
	}

}
