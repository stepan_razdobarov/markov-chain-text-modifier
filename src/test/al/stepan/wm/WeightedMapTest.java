package test.al.stepan.wm;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import al.stepan.wm.WeightedSet;


public class WeightedMapTest {

	private WeightedSet map;
	private String[] words = { "and", "no", "yes", "yes" };

	@Before
	public void setUp() throws Exception {
		map = new WeightedSet();
		feed(map, words);
	}

	@Test
	public void should_track_unique_words() {
		assertEquals(map.uniqueWords(), 3);
	}

	@Test
	public void should_track_total_weight() {
		assertEquals(map.totalWeights(), words.length);
	}

	@Test
	public void should_track_weight() {
		int yesCount = 0;
		for (int i = 1000000; i > 0; i--) {
			String word = map.getWeightedRandom();
			if (word.equals("yes")) {
				yesCount++;
			}
		}
		assertTrue(yesCount / 100000 == 5 || yesCount / 100000 == 4);
	}

	private void feed(WeightedSet map, String[] words) {
		for (String word : words) {
			map.add(word);
		}
	}

}
